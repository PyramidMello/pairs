require 'rails_helper'

describe 'Interns CRUD and validations' do
  before(:each) do
    if  page.driver.respond_to?(:browser) && page.driver.browser.respond_to?(:basic_authorize)
      page.driver.browser.basic_authorize(ENV["LOGIN"],ENV["PASSWORD"])
    end
    DatabaseCleaner.clean
  end

  let!(:intern) { Intern.create(first_name: 'John1',  last_name: 'Doran1') }

  context 'Interns CRUD' do
    it 'Create intern' do
      visit '/'
      assert page.has_content?('Interns')
      click_on 'New intern'
      fill_in 'intern_first_name', with: 'John'
      fill_in 'intern_last_name',  with: 'Dorian'
      click_on 'Create Intern'
      assert page.has_content?('Interns')
      expect(page).to have_content ('John')
      expect(page).to have_content ('Dorian')
    end

    it 'Modify intern' do
      visit '/'
      assert page.has_content?('Interns')
      click_on 'New intern'
      fill_in 'intern_first_name', with: 'John'
      fill_in 'intern_last_name',  with: 'Dorian'
      click_on 'Create Intern'
      assert page.has_content?('Interns')
      within(get_element_xpath('Dorian')) do
        click_on 'Edit'
      end
     fill_in 'intern_first_name', with: 'John2'
     fill_in 'intern_last_name', with: 'Dorian2'
     click_on 'Update Intern'
     assert page.has_content?('Interns')
     expect(page).to have_content ('John2')
     expect(page).to have_content ('Dorian2')
    end

    it 'Delete Intern' do
      visit '/'
      assert page.has_content?('Interns')
        within(get_element_xpath('John1')) do
          click_on 'Delete'
        end
      assert page.has_content?('Interns')
      expect(page).to have_content ('Intern was successfully destroyed.')
      expect(page).to_not have_content ('John1')
      end
  end

  context 'Interns validations' do
    it 'Create Intern with empty fields' do
      visit '/'
      assert page.has_content?('Interns')
      click_on 'New intern'
      fill_in 'intern_first_name', with:''
      fill_in 'intern_last_name', with:''
      click_on 'Create Intern'
      assert page.has_content?('New Intern')
      expect(page).to have_content ('can\'t be blank')
    end

    it "Shouldn't be able to create dublicate of students" do
      visit '/'
      assert page.has_content?('Interns')
      click_on 'New intern'
      fill_in 'intern_first_name', with:'Kevin'
      fill_in 'intern_last_name', with:'Smith'
      click_on 'Create Intern'
      assert page.has_content?("Interns")
      click_on 'New intern'
      fill_in 'intern_first_name', with:'Kevin'
      fill_in 'intern_last_name', with:'Smith'
      click_on 'Create Intern'
      assert page.has_content?('New Intern')
      expect(page).to have_content ("already been taken")
    end
  end


  context 'Random pair works properly' do
  let!(:intern) {
    Intern.create(first_name: 'Lasarus',  last_name: 'Exodus')
    Intern.create(first_name: 'Dimarzio',  last_name: 'Pickups')
    Intern.create(first_name: 'Kial',  last_name: 'Baert')
    Intern.create(first_name: 'Oern',  last_name: 'Dioaq')
    Intern.create(first_name: 'Disql',  last_name: 'Sazer')
    Intern.create(first_name: 'Anject',  last_name: 'Dunlop')
  }
    it "Shuffled logic works properly" do
      visit '/interns/shuffled'
      assert page.has_content?("Shuffled Interns")
      generated_sequence = []
      count_iteration = 600
      (1..count_iteration).each{ |x|
        click_on 'Repeat'
        generated_sequence << all('tbody tr .intern')[0].text
      }
      arr_counts = count_words(generated_sequence).to_a
      (0..arr_counts.size-1).each{|x|
         if (arr_counts[x][1] < (count_iteration/5)) && (arr_counts[x][1]>(count_iteration/7))
           req = true
         else
           req = false
         end
        expect(req).to eq (true)
      }
    end
  end
end
