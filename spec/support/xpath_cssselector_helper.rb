def get_element_xpath(contains)
  find(:xpath,"//tr[td//text()[contains(., '#{contains}')]]")
end
