def count_words(words)
  words.each_with_object(Hash.new 0) do |word, counter|
    counter[word] += 1
  end
end
