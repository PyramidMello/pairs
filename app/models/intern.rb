class Intern < ApplicationRecord
  validates_presence_of :first_name, :last_name
  validates :first_name, :last_name, length:{ minimum:3}
  validates :last_name, :uniqueness => { :scope => :first_name }

  def self.random_pairs!
    interns = all.to_a.shuffle
    raise ArgumentError.new('Oops error..') unless interns.size % 2 == 0
    return interns.each_slice(2).to_a
  end
end
