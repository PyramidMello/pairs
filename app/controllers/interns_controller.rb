class InternsController < ApplicationController
    before_action :set_intern, only: [:show, :edit, :update, :destroy]

    def index
      @interns = Intern.all
    end

    def shuffled
      @interns = Intern.random_pairs!
      rescue ArgumentError
        flash[:alert] = t('interns.cant_generate')
        redirect_to action: :new
    end

    def new
      @intern = Intern.new
    end

    def create
      @intern = Intern.new(intern_params)
      if @intern.save
        flash[:notice] = t('interns.created')
        redirect_to action: :index
      else
        render :new
      end
    end

    def update
        if @intern.update(intern_params)
          flash[:notice] = t('interns.updated')
          redirect_to action: :index
        else
          render :edit
        end
    end

    def destroy
      @intern.destroy
        flash[:notice] = t('interns.destroyed')
        redirect_to action: :index
    end

    private
      def set_intern
        @intern = Intern.find(params[:id])
      end

      def intern_params
        params.require(:intern).permit(:first_name, :last_name)
      end
end
