class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: ENV['LOGIN'], password: ENV['PASSWORD']
  protect_from_forgery with: :exception
end
