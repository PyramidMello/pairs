Rails.application.routes.draw do

  resources :interns do
    get :shuffled, on: :collection
  end

  root to: 'interns#index'
end
